﻿using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using Umbraco.Core.Models;
using Umbraco.Web;
using UnitTestingWorkshop.BusinessLogic.Services;
using UnitTestingWorkshop.BusinessLogic.Wrappers;

namespace UnitTestingWorkshop.Test.Exercise4
{
    [TestFixture]
    public class Exercise4Tests
    {
        WidgetService _widgetService;
       
        private Mock<UmbracoHelper> _umbracoHelperMock;

        // UNCOMMENT ME!
        // private Mock<IUmbracoHelperWrapper> _umbracoHelperMock;

        [SetUp]
        public void Setup()
        {
            _umbracoHelperMock = new Mock<UmbracoHelper>();
            _widgetService = new WidgetService(_umbracoHelperMock.Object);


            // UNCOMMENT ME!
            //_umbracoHelperMock = new Mock<IUmbracoHelperWrapper>();
            // _widgetService = new WidgetService(_umbracoHelperMock.Object);
        }

        [Test]
        public void GetPromotedContent_Returns_A_Result()
        {
            var containerNodeMock = new Mock<IPublishedContent>();

            var nodes = new List<IPublishedContent>() { new Mock<IPublishedContent>().Object };
            containerNodeMock.Setup(x => x.Children).Returns(nodes);

            _umbracoHelperMock.Setup(x => x.TypedContent(1234)).Returns(containerNodeMock.Object);
            var res = _widgetService.GetPromotedContent();

            Assert.IsNotEmpty(res);

        }


        [Test]
        public void GetPromotedContent_Returns_Empty_List_When_Container_Does_Not_Exist()
        {

            // UNCOMMENT ME!
            //_umbracoHelperMock.Setup(x => x.TypedContent(1234)).Returns((IPublishedContent)null);
            //var res = _widgetService.GetPromotedContent();

            //Assert.IsEmpty(res);
        }
    }
}
