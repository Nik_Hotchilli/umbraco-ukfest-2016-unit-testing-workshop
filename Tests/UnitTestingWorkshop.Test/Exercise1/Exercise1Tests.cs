﻿using NUnit.Framework;
using UnitTestingWorkshop.BusinessLogic.Services;

namespace UnitTestingWorkshop.Test.Exercise1
{
    [TestFixture]
    public class Exercise1Tests
    {
        private SettingsService _settingsService;
        [SetUp]
        public void Setup()
        {
            _settingsService = new SettingsService();
        }


        [Test]
        public void IsSimpleView_Returns_True_When_Cookie_Does_Not_Exist()
        {
            var res = _settingsService.IsSimpleView();

            Assert.True(res);
        }


        /*
         * UNCOMMENT ME!
         * 
        [Test]
        public void IsSimpleView_Returns_True_When_Cookie_Value_Is_1()
        {
            // setup
            var cookies = new HttpCookieCollection();
                 
            var cookie = new HttpCookie("Forum");
            cookie.Values.Add("ViewMode", "1");
            cookies.Add(cookie);
          
            // run the test          
            var res = _settingsService.IsSimpleView(cookies);

            Assert.True(res);
        }
        */



        [Test]
        public void IsSimpleView_Returns_False_When_Cookie_Value_Is_Not_1()
        {
           // FINISH ME
        } 

        
    }
}
