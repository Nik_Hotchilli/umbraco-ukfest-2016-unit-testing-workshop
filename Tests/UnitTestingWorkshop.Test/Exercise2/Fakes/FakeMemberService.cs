﻿using System;
using System.Collections.Generic;
using Umbraco.Core.Models;
using Umbraco.Core.Models.Membership;
using Umbraco.Core.Persistence.DatabaseModelDefinitions;
using Umbraco.Core.Persistence.Querying;
using Umbraco.Core.Services;

namespace UnitTestingWorkshop.Test.Exercise2.Fakes
{
    public class FakeMemberService : IMemberService
    {
        // UNCOMMENT ME! - Ex2, Task 3
        //public bool MemberExists;


        public IMember CreateMember(string username, string email, string name, string memberTypeAlias)
        {
             throw new NotImplementedException();

            
            // UNCOMMENT ME!
            // my fake return value
            //var member = new FakeMember();

            //return member;
        }



        public void Save(IMember entity, bool raiseEvents = true)
        {
            // We don't really care what happens here, as we're just dealing with a FakeMember
            // So we can remove the NotImplementedException
            
            // REMOVE ME!
            throw new NotImplementedException();
        }



        public void SavePassword(IMember member, string password)
        {
            // We don't really care what happens here, as we're just dealing with a FakeMember
            // So we can remove the NotImplementedException

            // REMOVE ME!
            throw new NotImplementedException();
        }



        public bool Exists(string username)
        {
            // UNCOMMENT ME!
            // return true;

            // REMOVE ME!
            throw new NotImplementedException();
        }


        #region Methods we don't need to care about for our tests
        public string GetDefaultMemberType()
        {
            throw new NotImplementedException();
        }

     

        public IMember CreateWithIdentity(string username, string email, string rawPasswordValue, string memberTypeAlias)
        {
            throw new NotImplementedException();
        }

        public IMember GetByProviderKey(object id)
        {
            throw new NotImplementedException();
        }

        public IMember GetByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public IMember GetByUsername(string login)
        {
            throw new NotImplementedException();
        }

        public void Delete(IMember membershipUser)
        {
            throw new NotImplementedException();
        }




        public void Save(IEnumerable<IMember> entities, bool raiseEvents = true)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IMember> FindByEmail(string emailStringToMatch, int pageIndex, int pageSize, out int totalRecords,
            StringPropertyMatchType matchType = StringPropertyMatchType.StartsWith)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IMember> FindByUsername(string login, int pageIndex, int pageSize, out int totalRecords,
            StringPropertyMatchType matchType = StringPropertyMatchType.StartsWith)
        {
            throw new NotImplementedException();
        }

        public int GetCount(MemberCountType countType)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IMember> GetAll(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public void AddRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetAllRoles(int memberId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetAllRoles(string username)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IMember> GetMembersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IMember> FindMembersInRole(string roleName, string usernameToMatch, StringPropertyMatchType matchType = StringPropertyMatchType.StartsWith)
        {
            throw new NotImplementedException();
        }

        public bool DeleteRole(string roleName, bool throwIfBeingUsed)
        {
            throw new NotImplementedException();
        }

        public void AssignRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public void AssignRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public void DissociateRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public void DissociateRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public void AssignRole(int memberId, string roleName)
        {
            throw new NotImplementedException();
        }

        public void AssignRoles(int[] memberIds, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public void DissociateRole(int memberId, string roleName)
        {
            throw new NotImplementedException();
        }

        public void DissociateRoles(int[] memberIds, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public IMember CreateMemberWithIdentity(string username, string email, IMemberType memberType)
        {
            throw new NotImplementedException();
        }
        


        public IMember CreateMember(string username, string email, string name, IMemberType memberType)
        {
            throw new NotImplementedException();
        }

        public IMember CreateMemberWithIdentity(string username, string email, string name, string memberTypeAlias)
        {
            throw new NotImplementedException();
        }

        public IMember CreateMemberWithIdentity(string username, string email, string name, IMemberType memberType)
        {
            throw new NotImplementedException();
        }


        public bool Exists(int id)
        {
            throw new NotImplementedException();
        }

        public IMember GetByKey(Guid id)
        {
            throw new NotImplementedException();
        }

        public IMember GetById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IMember> GetMembersByMemberType(string memberTypeAlias)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IMember> GetMembersByMemberType(int memberTypeId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IMember> GetMembersByGroup(string memberGroupName)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IMember> GetAllMembers(params int[] ids)
        {
            throw new NotImplementedException();
        }

        public void DeleteMembersOfType(int memberTypeId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IMember> FindMembersByDisplayName(string displayNameToMatch, int pageIndex, int pageSize, out int totalRecords,
            StringPropertyMatchType matchType = StringPropertyMatchType.StartsWith)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IMember> GetMembersByPropertyValue(string propertyTypeAlias, string value, StringPropertyMatchType matchType = StringPropertyMatchType.Exact)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IMember> GetMembersByPropertyValue(string propertyTypeAlias, int value, ValuePropertyMatchType matchType = ValuePropertyMatchType.Exact)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IMember> GetMembersByPropertyValue(string propertyTypeAlias, bool value)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IMember> GetMembersByPropertyValue(string propertyTypeAlias, DateTime value, ValuePropertyMatchType matchType = ValuePropertyMatchType.Exact)
        {
            throw new NotImplementedException();
        }

        public void RebuildXmlStructures(params int[] contentTypeIds)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IMember> GetAll(int pageIndex, int pageSize, out int totalRecords, string orderBy, Direction orderDirection, string memberTypeAlias = null, string filter = "")
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IMember> GetAll(long pageIndex, int pageSize, out long totalRecords, string orderBy, Direction orderDirection, string memberTypeAlias = null, string filter = "")
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IMember> GetAll(long pageIndex, int pageSize, out long totalRecords, string orderBy, Direction orderDirection, bool orderBySystemField, string memberTypeAlias, string filter)
        {
            throw new NotImplementedException();
        }

        public int Count(string memberTypeAlias = null)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IMember> FindMembersByDisplayName(string displayNameToMatch, long pageIndex, int pageSize, out long totalRecords, StringPropertyMatchType matchType = StringPropertyMatchType.StartsWith)
        {
            throw new NotImplementedException();
        }

        #endregion


    }
}
