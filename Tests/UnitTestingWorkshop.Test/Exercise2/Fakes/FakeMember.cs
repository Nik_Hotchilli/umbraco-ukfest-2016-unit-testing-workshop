﻿using System;
using System.Collections.Generic;
using Umbraco.Core.Models;

namespace UnitTestingWorkshop.Test.Exercise2.Fakes
{
    public class FakeMember : IMember
    {
        public object DeepClone()
        {
            throw new NotImplementedException();
        }

        public int Id { get; set; }
        public Guid Key { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public bool HasIdentity { get; private set; }
        public int CreatorId { get; set; }
        public int Level { get; set; }
        public string Name { get; set; }
        public int ParentId { get; set; }
        public string Path { get; set; }
        public int SortOrder { get; set; }
        public bool Trashed { get; private set; }
        public IDictionary<string, object> AdditionalData { get; private set; }
        public bool HasProperty(string propertyTypeAlias)
        {
            throw new NotImplementedException();
        }

        public object GetValue(string propertyTypeAlias)
        {
            throw new NotImplementedException();
        }

        public TPassType GetValue<TPassType>(string propertyTypeAlias)
        {
            throw new NotImplementedException();
        }

        public void SetValue(string propertyTypeAlias, object value)
        {
            throw new NotImplementedException();
        }

        public bool IsValid()
        {
            throw new NotImplementedException();
        }

        public void ChangeTrashedState(bool isTrashed, int parentId = -20)
        {
            throw new NotImplementedException();
        }

        public bool WasDirty()
        {
            throw new NotImplementedException();
        }

        public bool WasPropertyDirty(string propertyName)
        {
            throw new NotImplementedException();
        }

        public void ForgetPreviouslyDirtyProperties()
        {
            throw new NotImplementedException();
        }

        public void ResetDirtyProperties(bool rememberPreviouslyChangedProperties)
        {
            throw new NotImplementedException();
        }

        public bool IsDirty()
        {
            throw new NotImplementedException();
        }

        public bool IsPropertyDirty(string propName)
        {
            throw new NotImplementedException();
        }

        public void ResetDirtyProperties()
        {
            throw new NotImplementedException();
        }

        public int ContentTypeId { get; private set; }
        public Guid Version { get; private set; }
        public PropertyCollection Properties { get; set; }
        public IEnumerable<PropertyGroup> PropertyGroups { get; private set; }
        public IEnumerable<PropertyType> PropertyTypes { get; private set; }
        public object ProviderUserKey { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string RawPasswordValue { get; set; }
        public string PasswordQuestion { get; set; }
        public string RawPasswordAnswerValue { get; set; }
        public string Comments { get; set; }
        public bool IsApproved { get; set; }
        public bool IsLockedOut { get; set; }
        public DateTime LastLoginDate { get; set; }
        public DateTime LastPasswordChangeDate { get; set; }
        public DateTime LastLockoutDate { get; set; }
        public int FailedPasswordAttempts { get; set; }
        public string ContentTypeAlias { get; private set; }
        public IMemberType ContentType { get; private set; }
    }
}
