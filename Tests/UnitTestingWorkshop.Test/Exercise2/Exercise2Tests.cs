﻿using NUnit.Framework;
using UnitTestingWorkshop.BusinessLogic.Services;
using UnitTestingWorkshop.Test.Exercise2.Fakes;

namespace UnitTestingWorkshop.Test.Exercise2
{
    [TestFixture]
    public class Exercise2Tests
    {
        AccountService _accountService;
        private FakeMemberService _fakeMemberService;

        [SetUp]
        public void Setup()
        {
            _fakeMemberService = new FakeMemberService();

            _accountService = new AccountService(_fakeMemberService);
        }




        [Test]
        public void Create_Member_Success_Because_Member_Does_Not_Exist()
        {
            // UNCOMMENT ME! - Task 3
            //_fakeMemberService.MemberExists = false;
            
            var result = _accountService.Create("SomeName", "name@company.com", "password");

            Assert.AreEqual("success", result);
        }


        [Test]
        public void Create_Member_Fails_Because_Member_Exists()
        {
            // UNCOMMENT ME! - Task 3
            //_fakeMemberService.MemberExists = true;
            
            var result = _accountService.Create("SomeName", "name@company.com", "password");

            Assert.AreEqual("exists", result);
        }




    }
}
