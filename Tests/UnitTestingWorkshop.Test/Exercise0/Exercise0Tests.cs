﻿using System;
using NUnit.Framework;

namespace UnitTestingWorkshop.Test.Exercise0
{
    [TestFixture]
    public class Exercise0Tests
    {

        [SetUp]
        public void Setup()
        {
            // this is run automatically each time a test executes
        }


        [Test]
        public void GetBoolResult_Returns_True()
        {
            var helper = new MyHelperClass();
            var result = helper.GetBoolResult("1");

            Assert.True(result);

            // note: if you are comparing strings, you would do something like this Assert.AreEqual("The result", result);
        }


        [Test]
        public void GetBoolResult_Returns_False()
        {
            var helper = new MyHelperClass();
            var result = helper.GetBoolResult("0");

            Assert.False(result);
        }


        [Test]
       // [ExpectedException(typeof(Exception))]
        public void GetBoolResult_Throws_Exception_When_Input_Invalid()
        {
            var helper = new MyHelperClass();

            Assert.Throws<Exception>(() => helper.GetBoolResult("SiteCore"));
        }
    }
}
