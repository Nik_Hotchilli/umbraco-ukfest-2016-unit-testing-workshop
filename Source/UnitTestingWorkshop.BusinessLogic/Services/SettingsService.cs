﻿using System.Web;

namespace UnitTestingWorkshop.BusinessLogic.Services
{
    public interface ISettingsService
    {
        bool IsSimpleView();
    }


    public class SettingsService : ISettingsService
    {


        /// <summary>
        /// Returns if the forum view is in simple mode.
        /// </summary>
        public bool IsSimpleView()
        {
            var cookies = HttpContext.Current.Request.Cookies;

            var cookie = cookies["Forum"];

            if (cookie == null)
            {
                return true;
            }

            if (cookie.Values["ViewMode"] == "1")
            {
                return true;
            }

            return false;
        }



        ///// <summary>
        ///// Returns if the forum view is in simple mode.
        ///// </summary>
        //public bool IsSimpleView(HttpCookieCollection cookies)
        //{
        //    //var cookies = HttpContext.Current.Request.Cookies;

        //    var cookie = cookies["Forum"];

        //    if (cookie == null)
        //    {
        //        return true;
        //    }

        //    if (cookie.Values["ViewMode"] == "1")
        //    {
        //        return true;
        //    }

        //    return false;
        //}
    }
}
