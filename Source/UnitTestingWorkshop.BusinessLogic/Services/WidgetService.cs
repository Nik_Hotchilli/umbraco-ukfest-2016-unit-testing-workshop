﻿using System.Collections.Generic;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace UnitTestingWorkshop.BusinessLogic.Services
{
    public interface IWidgetService
    {
        IEnumerable<IPublishedContent> GetPromotedContent();
    }


    public class WidgetService : IWidgetService
    {
        private UmbracoHelper _umbracoHelper;
        
        // UNCOMMENT ME!
        //private IUmbracoHelperWrapper _umbracoHelper;


        public WidgetService(UmbracoHelper umbracoHelper)
        {
            _umbracoHelper = umbracoHelper;
        }


        // UNCOMMENT ME!
        //public WidgetService(IUmbracoHelperWrapper umbracoHelper)
        //{
        //    _umbracoHelper = umbracoHelper;
        //}



        public IEnumerable<IPublishedContent> GetPromotedContent()
        {
            var container = _umbracoHelper.TypedContent(1234);

            // UNCOMMENT ME!
            //if (container == null)
            //{
            //    return new List<IPublishedContent>();
            //}

            var widgets = container.Children;

            return widgets;
        }

    }
}
