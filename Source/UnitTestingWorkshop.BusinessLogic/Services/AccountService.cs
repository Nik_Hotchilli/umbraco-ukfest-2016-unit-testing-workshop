﻿using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace UnitTestingWorkshop.BusinessLogic.Services
{
    public class AccountService
    {

        private IMemberService _memberService;

    

        public AccountService(IMemberService memberService)
        {
            _memberService = memberService;
        }


        /// <summary>
        /// Create member.
        /// </summary>
        /// <returns></returns>
        public string Create(string name, string email, string plainTextPassword)
        {
            // UNCOMMENT ME! - Ex2, Task 2
            //// if member exists, return "exists"
            //var memberExists = _memberService.Exists(email);
            //if (memberExists)
            //{
            //    return "exists";
            //}

            var member = _memberService.CreateMember(email, email, name, "Member");
            _memberService.SavePassword(member, plainTextPassword);
            _memberService.Save(member);

            return "success";
        }



    }
}
