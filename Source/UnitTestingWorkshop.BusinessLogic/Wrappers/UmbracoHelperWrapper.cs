﻿using Umbraco.Core.Models;
using Umbraco.Web;

namespace UnitTestingWorkshop.BusinessLogic.Wrappers
{
    public interface IUmbracoHelperWrapper
    {
        IPublishedContent TypedContent(int id);
    }


    public class UmbracoHelperWrapper : IUmbracoHelperWrapper
    {
        private readonly UmbracoHelper _umbracoHelper;

        public UmbracoHelperWrapper(UmbracoHelper umbracoHelper)
        {
            _umbracoHelper = umbracoHelper;
        }


        public IPublishedContent TypedContent(int id)
        {
            return _umbracoHelper.TypedContent(id);
        }

    }
}
